import { Coverage } from '../entities/General.model';

export interface CoverageStateModel {
  coverages: Coverage[];
}
