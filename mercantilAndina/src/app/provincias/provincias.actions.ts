
export class InitProvincias {
  static readonly type = '[Provincias] Inicializar Provincias';
}

export class InitMunicipios {
  static readonly type = '[Provincias] Inicializar Municipios';
}
