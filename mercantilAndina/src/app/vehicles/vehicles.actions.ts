
export class InitVehiclesBrands {
  static readonly type = '[Vehicles] Inicializar Marcas de Vehiculos';
}

export class InitVehicleModels {
  static readonly type = '[Vehicles] Inicializar modelos del Vehiculo';
}

export class InitVehicleVersions {
  static readonly type = '[Vehicles] Inicializar versiones del Vehiculo';
}

export class InitVehicleYears {
  static readonly type = '[Vehicles] Inicializar años de Vehiculos';
}
